CC = gcc
NASM = nasm
QEMU = qemu
CCFLAGS = -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles -nodefaultlibs -Wall -Wextra
NASMFLAGS = -f elf32

kasm.o:
	$(NASM) $(NASMFLAGS) kernel/kernel.asm -o obj/kasm.o

kmain.o:
	$(CC) $(CCFLAGS) -c kernel/kernel.c -o obj/kmain.o

utils.o:
	$(CC) $(CCFLAGS) -c kernel/utils.c -o obj/utils.o

gdt.o:
	$(CC) $(CCFLAGS) -c kernel/gdt.c -o obj/gdt.o

idt.o:
	$(CC) $(CCFLAGS) -c kernel/idt.c -o obj/idt.o

isrs.o:
	$(CC) $(CCFLAGS) -c kernel/isrs.c -o obj/isrs.o

irq.o:
	$(CC) $(CCFLAGS) -c kernel/irq.c -o obj/irq.o

framebuffer.o:
	$(CC) $(CCFLAGS) -c kernel/framebuffer.c -o obj/framebuffer.o

timer.o:
	$(CC) $(CCFLAGS) -c kernel/device/timer.c -o obj/timer.o

keyboard.o:
	$(CC) $(CCFLAGS) -c kernel/device/keyboard.c -o obj/keyboard.o

all: kasm.o utils.o gdt.o idt.o isrs.o irq.o framebuffer.o timer.o keyboard.o kmain.o
	ld -m elf_i386 -T link.ld -o bin/kernel obj/kasm.o obj/utils.o obj/gdt.o obj/idt.o obj/isrs.o obj/irq.o obj/framebuffer.o obj/timer.o obj/keyboard.o obj/kmain.o

clean:
	rm obj/*.o

test:
	$(QEMU)-system-i386 -kernel bin/kernel
