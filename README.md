Oh My kernel
============

In order to compile run this commands.

* `nasm -f elf32 kernel.asm -o kasm.o`
* `gcc -m32 -c kernel.c -o kc.o`
* `ld -m elf_i386 -T link.ld -o kernel kasm.o kc.o`

Now you're ready to run it in emulator.

* `qemu-system-i386 -kernel kernel`

References
----------

[Let's write a kernel 101](http://arjunsreedharan.org/post/82710718100/kernel-101-lets-write-a-kernel)

License
-------

Permission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
