#ifndef __FRAMEBUFFER_H__
#define __FRAMEBUFFER_H__

#include "utils.h"

void fb_write(char *string);
void fb_write_hex(long long int number);
void fb_putch(char character);
void fb_init();

#endif
