#include "gdt.h"
#include "idt.h"
#include "isrs.h"
#include "irq.h"
#include "framebuffer.h"
#include "device/timer.h"
#include "device/keyboard.h"

void k_main() {
	gdt_install();
	idt_install();
	isrs_install();
	irq_install();
	timer_install();
	keyboard_install();
	fb_init();
	fb_write("Hello world!\n");
	fb_write("I am in second line. ");
	fb_write("Will it print correctly?\n");
	fb_write_hex((int)&k_main);
	//timer_wait(200);
	//fb_write("Now with keyboard support: ");
	for (;;);
}
