#include "timer.h"

unsigned long timer_ticks = 0;
const unsigned int FREQUENCY = 100;

static void timer_phase(int hz) {
  int divisor = 1193180 / hz;       // Calculate divisor
  outportb(0x43, 0x36);             // Set command byte 0x36
  outportb(0x40, divisor & 0xFF);   // Set low byte of divisor
  outportb(0x40, divisor >> 8);     // Set high byte of divisor
}

static void timer_handler() {
  timer_ticks++;
}

void timer_install() {
  timer_phase(FREQUENCY);
  irq_install_handler(0, timer_handler);
}

void timer_wait(int ticks) {
  unsigned long eticks;

  eticks = timer_ticks + ticks;
  while(timer_ticks < eticks);
}
