#ifndef __TIMER_H__
#define __TIMER_H__

#include "../utils.h"
#include "../irq.h"

void timer_install();
void timer_wait(int ticks);

#endif
