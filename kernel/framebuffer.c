#include "framebuffer.h"

const int GREEN_TEXT = 0x02;
const unsigned int LINES = 25;
const unsigned int COLUMNS = 80;
const unsigned int BYTES_PER_ELEMENT = 2;
const unsigned int SCREENSIZE = 20 * 80 * 2;
unsigned short *framebuffer;

unsigned int csr_x = 0;
unsigned int csr_y = 0;

static void move_cursor() {
    unsigned int tmp = csr_y * 80 + csr_x;

    outportb(0x3D4, 14);
    outportb(0x3D5, tmp >> 8);
    outportb(0x3D4, 15);
    outportb(0x3D5, tmp);
}

static void scroll_screen() {
  if (csr_y >= LINES) {
    for (unsigned int i = 0; i < (LINES - 1) * COLUMNS; i++) {
      framebuffer[i] = framebuffer[i + COLUMNS];
    }
    for (unsigned int i = (LINES - 1) * COLUMNS; i < LINES * COLUMNS; i++) {
      framebuffer[i] = 0x20 | (GREEN_TEXT << 8);
    }
    csr_y = 24;
  }
}

void fb_putch(char character) {
  if (csr_x == COLUMNS) {
    csr_x = 0;
    csr_y++;
  }
  switch (character) {
    case 17: // Up Arrow
      csr_y--;
      break;
    case 18: // Down Arrow
      csr_y++;
      break;
    case 19: // Left arrow
      csr_x--;
      break;
    case 20: // Righ arrow
      csr_x++;
      break;
    case '\n':
      csr_x = 0;
      csr_y++;
      break;
    case '\b':
      csr_x--;
      framebuffer[csr_y * COLUMNS + csr_x] = ' ';
      break;
    case '\r':
      csr_x = 0;
      break;
  }

  if (character >= ' ') {
    framebuffer[csr_y * COLUMNS + csr_x] = character | (GREEN_TEXT << 8);
    csr_x++;
  }
  scroll_screen();
  move_cursor();
}

void fb_write(char *string) {
  for (unsigned int i = 0; i < strlen(string); i++) {
    fb_putch(string[i]);
  }
}

// Have to reimplement using realloc and dynamic size array.
void fb_write_hex(long long int number) {
  char hex[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
                  'b', 'c', 'd', 'e', 'f'};
  char hex_num[17];
  int len = 0;
  while (number != 0) {
    if (len == 16) {
      fb_write("Number to large to convert!\n");
      return;
    }
    hex_num[len] = hex[number & 0xF];
    len++;
    number >>= 4;
  }
  for (int i = 0; i < len / 2; i++) {
    char tmp = hex_num[i];
    hex_num[i] = hex_num[len - i - 1];
    hex_num[len - i - 1] = tmp;
  }

  hex_num[len] = '\0';
  fb_write("0x");
  fb_write(hex_num);
  fb_putch('\n');
}

static void fb_clear() {
  for (unsigned int i = 0; i < SCREENSIZE; i++) {
    framebuffer[i] = 0x20 | (GREEN_TEXT << 8);
  }
  csr_x = 0;
  csr_y = 0;
  move_cursor();
}

void fb_init() {
  framebuffer = (unsigned short *)0xb8000;
  fb_clear();
}
