#ifndef __IDT_H__
#define __IDT_H__

#include "utils.h"

struct idt_entry {
  unsigned short base_low;
  unsigned short sel;
  unsigned char always0;
  unsigned char flags;
  unsigned short base_high;
} __attribute__((packed));

struct idt_ptr {
  unsigned short limit;
  struct idt_entry *base;
} __attribute__((packed));

struct idt_entry idt[256];
struct idt_ptr idtp;

// Function from kernel.asm
extern void idt_load();

void idt_set_gate(unsigned char num, unsigned long base,
                  unsigned short sel, unsigned char flags);
void idt_install();

#endif
