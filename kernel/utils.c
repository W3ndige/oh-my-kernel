#include "utils.h"

unsigned int strlen(char *string) {
  unsigned int length;
  if (string == NULL) {
    return 0;
  }
  for (length = 0; *string != '\0'; string++) {
    length++;
  }
  return length;
}

void *memset(void *buffer, char value, unsigned int size) {
    char *tmp = (char *)buffer;
    for (unsigned int i = 0; i < size; i++) {
      tmp[i] = value;
    }
    return tmp;
}

unsigned char inportb(unsigned short port) {
    unsigned char value;
    __asm__ __volatile__ ("inb %1, %0" : "=a" (value) : "dN" (port));
    return value;
}

void outportb(unsigned short port, unsigned char data) {
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (port), "a" (data));
}
