#ifndef __UTILS_H__
#define __UTILS_H__

#define NULL 0

unsigned int strlen(char *message);
void *memset(void *buffer, char value, unsigned int size);
unsigned char inportb(unsigned short port);
void outportb(unsigned short port, unsigned char data);

#endif
